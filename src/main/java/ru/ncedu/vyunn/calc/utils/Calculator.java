package ru.ncedu.vyunn.calc.utils;

import ru.ncedu.vyunn.calc.consolereader.*;
import ru.ncedu.vyunn.calc.operation.Operation;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * A class describing a calculator. <br/>
 * Stores information about possible operations {@link Operation}.
 * The behavior of the calculator is realized using a state pattern.
 * The calculator has 4 states :<br/>
 * 1) {@link StartState} The initial state  in which the calculator waits for the number to be entered.
 * The result of the previous operation is missing either because it was incorrectly
 * completed or because the calculator has not yet performed operations.<br/>
 * 2) {@link OperationState} The state in which the calculator is located after receiving the first operand.
 * It waits for the operation symbol to be entered.<br/>
 * 3) {@link NumberState} The state in which the calculator has the first operand and knows the operation to be performed.
 * It waits for the second operand to be entered. In this state, the calculator will find the result
 * of the operation and outputs it to the console.<br/>
 * 4) {@link NumberOperationState} The state in which calculator has result of previous operation and
 * it can use it as first operand of the next operation. At the same time calculator read new value of
 * the first operand. So it waits for number or symbol of operation.
 * @see State
 * @see StartState
 * @see OperationState
 * @see NumberState
 * @see NumberOperationState
 * @see Operation
 */
public class Calculator {

    /**
     * A Map storing pairs of operation symbols and classes which implements this operations.
     */
    private Map<String, Operation> operationMap;

    /**
     * Current state of the calculator.
     */
    private State state;

    /**
     * Loads information about operations that implements calculator.<br/>
     * Example of config file:
     * ru.ncedu.vyunn.calc.operation.Division;/ <br/>
     * class which implements operation and symbol of this operation are separated by ";".
     * @param fileName - name of config file.
     * @throws Exception - some problems with loading.
     */
    public void init(String fileName) throws Exception {
        Map<String, Operation> operationMap = new HashMap<>();

        state = new StartState(this);

        try (Scanner scanner = new Scanner(new File(fileName))) {

            while (scanner.hasNext()) {
                String[] option = scanner.nextLine().split(";");
                Operation operation =
                        (Operation) Class.forName(option[0])
                                .newInstance();
                operationMap.put(option[1], operation);
            }
            this.operationMap = operationMap;

        } catch (IOException | IllegalAccessException
                | InstantiationException | ClassNotFoundException e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * Handles the next line of input.
     * @return true if calculator is ready to continue work and false if is not.
     */
    public boolean next() {
        return state.readNext();
    }

    /**
     * Sets new state to calculator.
     * @param state new state.
     */
    public void setState(State state) {
        this.state = state;
    }

    /**
     * Checks if calculator knows symbol of operation.
     * @param string new state.
     * @return true if calculator contains it and false if do not.
     */
    public boolean containsOperation(String string) {
        return operationMap.keySet().contains(string);
    }

    /**
     * Returns class that implements operation with symbol string.
     * @param string new state.
     * @return object which implements this operation.
     */
    public Operation getOperationBySymbol(String string) {
        return operationMap.get(string);
    }
}
