package ru.ncedu.vyunn.calc.consolereader;

/**
 * The state in which the calculator is located after receiving the first operand.<br/>
 * It waits for the operation symbol to be entered.
 * @see State
 */
public class OperationState extends State {

    /**
     * @param state previous state of the calculator.
     */
    public OperationState(State state) {
        super(state.getPreviousValue(), null, state.getCalculator());
    }

    /**
     * Processes new line of input.
     * In this state, the calculator waits for the next operation symbol.
     * The calculator goes to state <code>NumberState</code> After it received.
     * @return <code>true<code/> if calculator is ready for the next input or
     * <code>false<code/> if is not (user wrote "exit").
     * @see NumberState
     */
    @Override
    public boolean readNext() {
        String string = readLine("Enter the operation: ");
        if (string == null) {
            return false;
        }

        if (containsOperation(string)) {
            setLastOperation(getOperationBySymbol(string));
            updateState(new NumberState(this));
        } else {
            System.out.println("Invalid operation! Try again.");
            return readNext();
        }

        return true;
    }
}
