package ru.ncedu.vyunn.calc.consolereader;

import ru.ncedu.vyunn.calc.utils.Calculator;

/**
 * The initial state  in which the calculator waits for the number to be entered.<br/>
 * The result of the previous operation is missing either because it was incorrectly
 * completed or because the calculator has not yet performed operations.
 * @see State
 */
public class StartState extends State {

    /**
     * @param calculator calculator which state it describes.
     */
    public StartState(Calculator calculator) {
        super(null, null, calculator);
    }

    /**
     * Processes new line of input.
     * In this state calculator is waiting for the numeric input.
     * The calculator goes to state <code>OperationState</code> After it received.
     * @return <code>true<code/> if calculator is ready for the next input or
     * <code>false<code/> if is not (user wrote "exit").
     * @see OperationState
     */
    @Override
    public boolean readNext() {
        String string = readLine("Enter the number: ");
        if (string == null) {
            return false;
        }
        try {
            this.setPreviousValue(Double.parseDouble(string));
            updateState(new OperationState(this));
        } catch (NumberFormatException e) {
            System.out.println("Invalid number format! Try again.");
            return readNext();
        }
        return true;
    }
}
