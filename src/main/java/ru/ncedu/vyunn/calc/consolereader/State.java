package ru.ncedu.vyunn.calc.consolereader;

import ru.ncedu.vyunn.calc.operation.Operation;
import ru.ncedu.vyunn.calc.utils.Calculator;

import java.util.Scanner;

/**
 * A class describing a states of calculator.<br/>
 * Class stores information about previous operations as result of last operation
 * <code>previousValue<code/> and object which implements realisation of current operation if it do not
 * end yet <code>lastOperation<code/> {@link Operation}.<br/>
 * Class stores a reference to the object of the calculator whose state it describes <code>calculator<code/> {@link Calculator}.<br/>
 * The calculator has 4 states :<br/>
 * 1) {@link StartState} The initial state  in which the calculator waits for the number to be entered.
 * The result of the previous operation is missing either because it was incorrectly
 * completed or because the calculator has not yet performed operations.<br/>
 * 2) {@link OperationState} The state in which the calculator is located after receiving the first operand.
 * It waits for the operation symbol to be entered.<br/>
 * 3) {@link NumberState} The state in which the calculator has the first operand and knows the operation to be performed.
 * It waits for the second operand to be entered. In this state, the calculator will find the result
 * of the operation and outputs it to the console.<br/>
 * 4) {@link NumberOperationState} The state in which calculator has result of previous operation and
 * it can use it as first operand of the next operation. At the same time calculator read new value of
 * the first operand. So it waits for number or symbol of operation.
 * @see State
 * @see StartState
 * @see OperationState
 * @see NumberState
 * @see NumberOperationState
 * @see Operation
 * @see Calculator
 */
public abstract class State {

    /**
     * Reference to the object of the calculator which state it describes.
     */
    private Calculator calculator;

    /**
     * Result of previous operation or previous operand.
     */
    private Double previousValue;
    /**
     * Last unfinished operation.
     */
    private Operation lastOperation;

    /**
     * @param previousValue result of previous operation or last entered operand.
     * @param lastOperation class which implements las entered operation.
     * @param calculator calculator which state it describes.
     */
    public State(Double previousValue, Operation lastOperation, Calculator calculator) {
        this.previousValue = previousValue;
        this.lastOperation = lastOperation;
        this.calculator = calculator;
    }

    /**
     * Reads and processes next line of input.
     */
    public abstract boolean readNext();

    /**
     * Returns reference to the calculator which state describes this object.
     * @return reference to the calculator object.
     */
    public Calculator getCalculator() {
        return calculator;
    }

    /**
     * Returns last unfinished operation.
     * @return last unfinished operation or <code>null<code/> if there is no unfinished operations.
     */
    public Operation getLastOperation() {
        return lastOperation;
    }

    /**
     * Sets new last unfinished operation.
     * @param lastOperation last unfinished operation.
     */
    public void setLastOperation(Operation lastOperation) {
        this.lastOperation = lastOperation;
    }

    /**
     * Returns result of last operation or previous operand.
     * @return result of last operation or previous operand. <code>null<code/> if there is no that information.
     */
    public Double getPreviousValue() {
        return previousValue;
    }

    /**
     * Sets result of last operation or previous operand.
     * @param previousValue result of last operation or previous operand or <code>null<code/>.
     */
    public void setPreviousValue(Double previousValue) {
        this.previousValue = previousValue;
    }

    /**
     * Returns operation of calculator by symbol of this operation (like "*" or "/").
     * @param string symbol of operation.
     * @return class implementing this operation or <code>null<code/> if there is no that.
     */
    public Operation getOperationBySymbol(String string) {
        return getCalculator().getOperationBySymbol(string);
    }

    /**
     * Checks whether the calculator supports the operation specified by its symbol.
     * @param string symbol of operation.
     * @return <code>true<code/> if supports or <code>false<code/> if do not.
     */
    public boolean containsOperation(String string) {
        return getCalculator().containsOperation(string);
    }

    /**
     * Initial processing of the next line of input. Displays a message with information about the required input.
     * Reads the next line and checks if the user wants to close the calculator.
     * @param greeting message which will be displayed.
     * @return new line of input or <code>null<code/> if used wrote exit (wants to close calculator).
     */
    public String readLine(String greeting) {
        System.out.print(greeting);
        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();
        if (string.equals("exit")) {
            System.out.println("Closing...");
            return null;
        }
        return string;
    }

    /**
     * Update state of calculator which state it describes.
     * @param state new state.
     */
    public void updateState(State state) {
        getCalculator().setState(state);
    }
}
