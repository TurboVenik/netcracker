package ru.ncedu.vyunn.calc.consolereader;

/**
 * The state in which the calculator has the first operand and knows the operation to be performed.<br/>
 * It waits for the second operand to be entered. In this state, the calculator will find the result
 * of the operation and outputs it to the console.
 * @see State
 */
public class NumberState extends State {

    /**
     * @param state previous state of the calculator.
     */
    public NumberState(State state) {
        super(state.getPreviousValue(), state.getLastOperation(), state.getCalculator());
    }

    /**
     * Processes new line of input.
     * In this state calculator is waiting for the numeric input.
     * The calculator shows result of current operation if it finished correct and
     * goes to state <code>NumberOperationState</code> when do that.
     * @return <code>true<code/> if calculator is ready for the next input or
     * <code>false<code/> if is not (user wrote "exit").
     * @see NumberState
     */
    @Override
    public boolean readNext() {
        String string = readLine("Enter the number: ");
        if (string == null) {
            return false;
        }

        try {
            Double answer = getLastOperation().calculate(getPreviousValue(), Double.parseDouble(string));
            if (answer == null) {
                updateState(new StartState(getCalculator()));
                return true;
            }
            System.out.println(String.format("Answer: %s", answer));
            this.setPreviousValue(answer);

            updateState(new NumberOperationState(this));
        } catch (NumberFormatException e) {
            System.out.println("Invalid number format! Try again.");
            return readNext();
        }
        return true;
    }
}
