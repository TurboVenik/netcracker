package ru.ncedu.vyunn.calc.consolereader;

/**
 * The state in which calculator has result of previous operation and it can use it
 * as first operand of the next operation.<br/>
 * At the same time calculator read new value of
 * the first operand. So it waits for number or symbol of operation.
 * @see State
 */
public class NumberOperationState extends State {

    /**
     * @param state previous state of the calculator.
     */
    public NumberOperationState(State state) {
        super(state.getPreviousValue(), null, state.getCalculator());
    }

    /**
     * Processes new line of input.
     * In this state calculator is waiting for the numeric input or symbol of operation.
     * If number was received calculator goes to <code>OperationState</code>.
     * If operation symbol was received calculator goes to <code>NumberState</code>
     * @return <code>true<code/> if calculator is ready for the next input or
     * <code>false<code/> if is not (user wrote "exit").
     * @see NumberState
     * @see OperationState
     */
    @Override
    public boolean readNext() {
        String string = readLine("Enter the number or operation: ");
        if (string == null) {
            return false;
        }

        if (containsOperation(string)) {
            setLastOperation(getOperationBySymbol(string));
            updateState(new NumberState(this));
            return true;
        } else {
            try {
                this.setPreviousValue(Double.parseDouble(string));
                updateState(new OperationState(this));
            } catch (NumberFormatException e) {
                System.out.println("Invalid number or operation format! Try again.");
                return readNext();
            }
        }
        return true;
    }
}
