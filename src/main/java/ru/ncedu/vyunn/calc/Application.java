package ru.ncedu.vyunn.calc;

import ru.ncedu.vyunn.calc.utils.Calculator;

import java.io.IOException;

public class Application {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        try {
            calculator.init("src/main/resources/calculator.conf");
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        while (calculator.next()) {
        }
    }
}
