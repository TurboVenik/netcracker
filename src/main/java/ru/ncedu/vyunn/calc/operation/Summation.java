package ru.ncedu.vyunn.calc.operation;

/**
 *Class <code>Summation</code> implements the operation of summing two numbers.
 */
public class Summation implements Operation {

    /**
     * The method finds the sum of two operands.
     * @param a two operands are expected.
     * @return result of operation.
     */
    public Double calculate(double... a) {
        if (a.length < 2) {
            throw new IllegalArgumentException();
        }
        return a[0] + a[1];
    }
}
