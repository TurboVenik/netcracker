package ru.ncedu.vyunn.calc.operation;

/**
 * The <code>Operation<code/> interface should be implemented by any class which
 * implements a calculator operation.
 */
public interface Operation {

    /**
     * A method that describes the operation of a calculator.
     * @param a massive of operand of the operation.
     * @return result of operation or <code>null</code> if operands are illegal.
     */
    Double calculate(double... a);
}
