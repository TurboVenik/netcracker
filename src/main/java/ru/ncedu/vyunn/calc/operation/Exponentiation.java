package ru.ncedu.vyunn.calc.operation;

/**
 *Class <code>Exponentiation<code/> implements the operation of raising a number to a power.
 */
public class Exponentiation implements Operation {

    /**
     * The method raises the first operand to the power of the second.
     * @param a two operands are expected.
     * @return result of operation.
     */
    public Double calculate(double... a) {
        if (a.length < 2) {
            throw new IllegalArgumentException();
        }
        return Math.pow(a[0], a[1]);
    }
}
