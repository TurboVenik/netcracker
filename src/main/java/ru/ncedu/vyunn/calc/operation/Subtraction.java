package ru.ncedu.vyunn.calc.operation;

/**
 *Class <code>Subtraction</code> implements the operation of dividing two numbers.
 */
public class Subtraction implements Operation {

    /**
     * The method finds the difference between the first and second operands.
     * @param a two operands are expected.
     * @return result of operation.
     */
    public Double calculate(double... a) {
        if (a.length < 2) {
            throw new IllegalArgumentException();
        }
        return a[0] - a[1];
    }
}
