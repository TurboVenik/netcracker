package ru.ncedu.vyunn.calc.operation;

/**
 *Class <code>Multiplication</code> implements the operation of multiplying two numbers.
 */
public class Multiplication implements Operation {

    /**
     * The method finds the multiplication of two operands.
     * @param a two operands are expected.
     * @return result of operation.
     */
    public Double calculate(double... a) {
        if (a.length < 2) {
            throw new IllegalArgumentException();
        }
        return a[0] * a[1];
    }
}
