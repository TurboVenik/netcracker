package ru.ncedu.vyunn.calc.operation;

/**
 *Class <code>Division</code> that implements the operation of division.
 */
public class Division implements Operation {

    /**
     * The method that finds the quotient of dividing the first operand by the second.
     * @param a two operands are expected.
     * @return result of operation or <code>null</code> if operands are illegal.
     */
    public Double calculate(double... a) {
        if (a.length < 2) {
            throw new IllegalArgumentException();
        }
        double div = a[0] / a[1];
        if (Double.isInfinite(div)) {
            System.out.println("Error! Division by 0.");
            return null;
        }
        if (Double.isNaN(div)) {
            System.out.println("It seems you got something indeterminate.");
            return null;
        }
        return div;
    }
}
